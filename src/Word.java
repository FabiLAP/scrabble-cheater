import java.util.HashSet;
import java.util.Random;

public class Word {
   	
   	//fields
   	private String value;	//this is the word
   	private long normalizedValue;	//this is the id of the normalized word
   	private Word next = null;	//reference to the next word
 
   	/*
   	 * constructor for initializing
   	 */
   	public Word(String value, long normalizedKeyValue, Word next) {
         	// TODO Auto-generated constructor stub
         	this.value = value;
         	this.next = next;
         	this.normalizedValue=normalizedKeyValue;
   	}
   	
   	/*
   	 * returns the word as a String
   	 */
   	public String getValue() {
         	return value;
   	}
   	
   	/*
   	 * returns the normalized id value of the words
   	 */
   	public long getNormalizedKeyValue() {
     	return normalizedValue;
	}
 
   	/*
   	 * returns the next word
   	 */
   	public Word getNext() {
         	return next;
   	}
   	
   	/*
   	 * setting the next word
   	 */
   	public void setNext(Word setNext) {
         	this.next = setNext;
   	}
   	
   	/*
   	 * Method for returning a String representation (for printing)
   	 * @see java.lang.Object#toString()
   	 */
   	public String toString() {
         	// TODO Auto-generated method stub
         	return (String) value +" (id: "+normalizedValue+", points: "+getScore()+")";
   	}
   	
   	public int getScore(){
   		int score = 0;
   		
   		char[] wordAsCharArray = this.value.toCharArray();
		for (int i=0; i<wordAsCharArray.length; i++){
			char letter = wordAsCharArray[i];
			if(letter=='e' || letter=='a' || letter=='i' || letter=='o' || letter=='n' ||
			   letter=='r' || letter=='t' || letter=='l' || letter=='s' || letter=='u'){
				score+=1;				
			}
			if(letter=='d' || letter=='g'){
				 score+=2;
			}
			if(letter=='b' || letter=='c' || letter=='m' || letter=='p'){
				 score+=3;
			}
			if(letter=='f' || letter=='h' || letter=='v' || letter=='w'	|| letter=='y'){
				 score+=4;
			}
			if(letter=='k'){
				 score+=5;
			}
			if(letter=='j' || letter=='x'){
				 score+=8;
			}
			if(letter=='q' || letter=='z'){
				 score+=10;
			}
		}   		
   		
   		return score;
   	}
   	
   	public static String getARandomString (int size){
   		String toreturn = "";
   		Random random = new Random();
   		
   		for (int i = 0; i<size; i++){
   			
   			//range of 97 - 122
   			int randomnumber = random.nextInt(26)+97;
   			//as char
   			char corrspondingRandomChar = (char)randomnumber;
   			//add to return string
   			toreturn+=corrspondingRandomChar;
   		}   		
   		return toreturn;
   	}
   	
   	/*
   	 * returns any constellation of the letters in a string
   	 * no permutations of the order!!!
   	 */
   	public static HashSet<String> getAllPermutationsOfAString (HashSet<String> tocheck, HashSet<String> finalwords){
   		
   		for(String word: tocheck){
   			
   			char[] wordAsCharArray = word.toCharArray();
   			HashSet<String> cache = new HashSet<String>();
   	   		
   	   		for (int i = 0; i<wordAsCharArray.length;i++){
   				String newWord = "";
   				for (int j = 0; j<wordAsCharArray.length;j++){
   					if(j!=i){
   						newWord+=wordAsCharArray[j];
   					}
   				}
   				finalwords.add(newWord);
   				cache.add(newWord);
   			}	 
   	   		
   	   		getAllPermutationsOfAString(cache,finalwords);   			
   		}   		
   		return finalwords;
   	}
}