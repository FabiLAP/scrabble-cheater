import java.util.HashSet;


public class Statistic {

	public static void analyse (Dict dict){

		System.out.println();
		System.out.println("##########");
		System.out.println("STATISTICS");
		System.out.println("##########");
		
		int numberOfWords = 0;
		int numberOfChains = 0;
		int longestChain = 0;
		int shortestChain = Integer.MAX_VALUE;
		int numberOfEmptyChains = 0;
		int averageWordsPerChain;
		
		
		//Iterate through the hashtable/chains
		for (int i = 0; i<dict.hashtable.length; i++){
			
			//increase numberOfChains
			numberOfChains++;
			
			//compare and check for longest chain
			if(dict.hashtable[i].size()>longestChain){
				longestChain=dict.hashtable[i].size();
			}			
			//compare and check for shortest chain
			if(dict.hashtable[i].size()<shortestChain){
				shortestChain=dict.hashtable[i].size();
			}			
			//compare and check for empty chain
			if(dict.hashtable[i].size()==0){
				numberOfEmptyChains++;
			}
			
			//Iterate through the chain of words at this hashposition
			HashSet<Word> currentChain = dict.hashtable[i].getWords();
			for (Word aWord: currentChain){
				numberOfWords++;
			}
		}
		
		averageWordsPerChain = numberOfWords / numberOfChains;
		
		//printing results
		System.out.println("Number of words: "+numberOfWords);
		System.out.println("Longest chain has "+longestChain+" entries");
		System.out.println("Shortest chain has "+shortestChain+" entries");
		System.out.println("Average number of words per chain: "+averageWordsPerChain);
		System.out.println("Number of empty chains: "+numberOfEmptyChains);
		System.out.println("Overall number of chains: "+numberOfChains);	
		System.out.println("##########");
	}
}
