import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashSet;

public class Application {
	public static void main(String[] args) {
		
		Dict dict = new Dict(30000);
		
		try {
			System.out.println("adding words ...");
			dict.addWordsFromFile("bin/2.txt");
			dict.addWordsFromFile("bin/3.txt");
			dict.addWordsFromFile("bin/4.txt");
			dict.addWordsFromFile("bin/5.txt");
			dict.addWordsFromFile("bin/6.txt");
			dict.addWordsFromFile("bin/7.txt");
			dict.addWordsFromFile("bin/8.txt");
			dict.addWordsFromFile("bin/9.txt");
			dict.addWordsFromFile("bin/10.txt");
			dict.addWordsFromFile("bin/11.txt");
			dict.addWordsFromFile("bin/12.txt");
			dict.addWordsFromFile("bin/13.txt");
			dict.addWordsFromFile("bin/14.txt");
			dict.addWordsFromFile("bin/15.txt");
			System.out.println("adding words done");

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		Statistic.analyse(dict);	
//		System.out.println();
//		String randomString = Word.getARandomString(7);
//		System.out.println("Random generated String: "+randomString);
//		System.out.println("checking this one for words");
//		System.out.println(dict.getPermutationsAsString(randomString));
//		
////		//Checking other smaller words
//		HashSet<String> otherShorterStringsforRandom = new HashSet<String>();
//		
//		//input string have to be stored in a set - usage of recursive method
//		HashSet<String> randomNumberAsHashset = new HashSet<String>();
//		randomNumberAsHashset.add(randomString);
//		
//		Word.getAllPermutationsOfAString(randomNumberAsHashset, otherShorterStringsforRandom);
//		for (String aVariant: otherShorterStringsforRandom){
//			//the getPermutationsAsString checks whether the variant is a true word!
//			System.out.print(dict.getPermutationsAsString(aVariant));
//		}
//				
//		HashSet<String> tocheck = new HashSet<String>();
//		tocheck.add("java");
//		HashSet<String> finalStrings = new HashSet<String>();
//		
//		HashSet<String> allvariations = Word.getAllPermutationsOfAString(tocheck, finalStrings);
//		for (String anVariantion: allvariations){
//			System.out.print(anVariantion.toString()+" ");
//		}
//		
//		//+++++++++++++++++++++++++++++++++++++
//		System.out.println();
//		
//		HashSet<String> tocheck2 = new HashSet<String>();
//		tocheck2.add("hello");
//		HashSet<String> finalStrings2 = new HashSet<String>();
//		
//		Word.getAllPermutationsOfAString(tocheck2, finalStrings2);
//		for (String anVariantion: finalStrings2){
//			System.out.print(anVariantion.toString()+" ");
//		}
		//++++++++++++++++++++++++++++++++++++++++++
		
//		System.out.println(dict.isAWord("haw"));
		
		//####### INPUT Stuff		
//		//###########################################
		BufferedReader userin = new BufferedReader(new InputStreamReader(System.in));
		String userInput;
		System.out.println();
		System.out.println();
		System.out.println("######################################");
		System.out.println("Welcome to the SCRABBLE CHEATER alpha!");
		System.out.println("Type 'bye' or 'exit' to quit.");
		System.out.println("Please insert your letters.");
		try {
			while ((userInput = userin.readLine()) != null) {

				//Termination!
				if(userInput.equals("bye")||userInput.equals("exit")){
					System.out.println("Im feeling good");
					System.exit(0);
				}
				
				//check if there is a permutation of the input for the exact number of the given letters
				System.out.println(dict.getPermutationsAsString(userInput));
				System.out.print("---------------------------");
				
				//Checking other smaller words
				HashSet<String> otherShorterStrings = new HashSet<String>();
				
				//input string have to be stored in a set - usage of recursive method
				HashSet<String> inputAsHashset = new HashSet<String>();
				inputAsHashset.add(userInput);
				
				Word.getAllPermutationsOfAString(inputAsHashset, otherShorterStrings);
				for (String aVariant: otherShorterStrings){
					//the getPermutationsAsString checks whether the variant is a true word!
					System.out.print(dict.getPermutationsAsString(aVariant));
				}
				System.out.println("");
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		//###########################################
	}
}
