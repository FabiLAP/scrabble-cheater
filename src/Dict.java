import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Arrays;
import java.util.HashSet;


public class Dict {

	//fields
	private int m;	//size of hashtable
	public Chain[] hashtable;	//storage for the words (public - for statistics!!)
	
	/*
	 * Constructor 
	 * Initializes the hashtable and fills them with empty chains
	 */
	public Dict(int m) {
		// TODO Auto-generated constructor stub
		this.m=m;
		this.hashtable=new Chain[m];
		//init Linkedlists
		for (int i=0; i<hashtable.length; i++){
			Chain aChain = new Chain();
			hashtable[i] = aChain;
		}
	}
	
	/*
	 * This method adds a single word to the hashtable.
	 * Therefore the word gets normalized ("hello"-->"ehllo"), then the
	 * id of this will generated and saved at the position by using 
	 * the getHashValue() for this id.
	 * 
	 * Attention: the normalization is only for indexing and for the id.
	 * The original word is saved!
	 */
	public void addWord(String word){		
		long normalizedIdValue = this.getId(this.normalize(word));			
		//############################################################
		//NORMALIZED BEFORE GENERATING THE HASHINDEX!!!!!!!!!!!!!!!!!!!!!!
		int index = this.getHashValue(this.normalize(word));
		//saving!
		hashtable[index].add(word, normalizedIdValue);
	}
	
	/*
	 * This method adds words from the argument (a set) directly to 
	 * the hashtable
	 */
	public void addWords(HashSet<String> words){
		for (String word: words){
			this.addWord(word);
		}
	}
	
	/*
	 * This method adds words from a file directly to the hashtable
	 */
	public void addWordsFromFile(String filename) throws Exception{
		HashSet<String> words = this.readWordsFromFile(filename);		
//		System.out.println("adding words ...");		
		this.addWords(words);
//		System.out.println("adding words done");
		
	}
	
	/*
	 * This method returns a set of words which are given in file.
	 * The argument declares the path
	 */
	public HashSet<String> readWordsFromFile(String filename) throws Exception{
		
//		System.out.println("reading ...");
		
		Reader reader = null;
		HashSet<String> words = new HashSet<>();
		try{
			reader = new FileReader(filename);
			String wordCache = "";
			for (int c; (c = reader.read() ) != -1;){	
				if(c!=32){
					wordCache+=(char)c;
				}
				else{
					//whitespace!
					words.add(wordCache.toLowerCase());
					wordCache="";
				}		
			}						
//            System.out.println("reading done");
		}
		catch ( IOException e ) {
		  System.err.println( "Sorry dude!" );
		}
		finally {
		  try { reader.close(); } catch ( Exception e ) { e.printStackTrace(); }
		}		
		
		return words;
	}
	
	/*
	 * the method for normalizing a String. This orders all letters
	 * in the String in a alphabetical way.
	 * For example: "Hallo" --> "ahllo"
	 */
	public String normalize(String toNormalize){
		char[] todo = toNormalize.toLowerCase().toCharArray();		
		//sort them in an alphabetical way
		Arrays.sort(todo);				
		return (new String(todo));
	}

	/*
	 * This method generates a Hashcode for indexing a String
	 * Uses the ID of a String and the size of the hashtable
	 */
	public int getHashValue(String toHash){		
		long indexValue = getId(toHash)%m;
		return (int)indexValue;
	}
	
	/*
	 * method for generating an id for a String
	 * for example: "abc" id = a*26^2 + b*26^1 + c*26^0
	 */
	public long getId(String word){
		long id=0;
		char[] wordAsCharArray = word.toCharArray();
		for (int i=0; i<wordAsCharArray.length; i++){
			int numericvalue = ((int)wordAsCharArray[i]-96);
			id += numericvalue * Math.pow(26, wordAsCharArray.length-i-1);
		} 
		return id;		
	}
	
	/*
	 * This method returns a set of all words which are stored in a chain
	 * of the hashtable at particular position. This position is the same
	 * as the String from the argument.
	 */
	public HashSet<Word> getWordsAtHashLocation(String word){
		int indexOfInput = this.getHashValue(this.normalize(word));
		return hashtable[indexOfInput].getWords();
	}
	
	/*
	 * This method returns all available permutations for the InputString as a set
	 */
	public HashSet<Word> getPermutations(String word){
		
		HashSet<Word> words = getWordsAtHashLocation(word);
		HashSet<Word> permutations = new HashSet<Word>();
		
		long normalizedHashValueFromInput = this.getId(this.normalize(word));		

		for (Word possiblePermutation: words){
			if(possiblePermutation.getNormalizedKeyValue()==normalizedHashValueFromInput){
				permutations.add(possiblePermutation);
			}
		}		
		return permutations;
	}
	
	/*
	 * returns a true if the 2 strings are permutations
	 * 
	 * not tested
	 */
	public boolean isPermutation (String a, String b){
		return this.getId(a)==this.getId(b);
	}
	
	/*
	 * checks if the string is available in the dict!
	 */
	public boolean isAWord(String input){
		long normalizedHashValueFromInput = this.getId(this.normalize(input));		
		HashSet<Word> words = getWordsAtHashLocation(input);
		
		for (Word possiblePermutation: words){
			if(possiblePermutation.getNormalizedKeyValue()==normalizedHashValueFromInput){
				return true;
			}
		}		
		return false;
	}
	
	/*
	 * returns a nice String for printing the whole hashtable
	 * @see java.lang.Object#toString()
	 */
	public String toString(){
		String toReturn = "";
		for (int i=0; i<hashtable.length; i++){
			toReturn += i +": "+ hashtable[i].toString()+"\n";
		}
		return toReturn;
	}
	
	/*
	 * returns all possible permutations for the InputString as a String
	 */
	public String getPermutationsAsString(String word){
		String toReturn = "";
		HashSet<Word> permutations = this.getPermutations(word);
		
		if (permutations.isEmpty()){
//			return "Sorry, this isnt in the dictionary.";
		}
		else{
			for (Word aWord: permutations){
				toReturn += "\n"+aWord.toString();
			}
		}
		return toReturn;
	}
}