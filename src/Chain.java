import java.util.HashSet;

public class Chain {
 
   	//fields
   	public Word first = null;
   	
   	public Chain() {
         	// constructor
   	}
   	
   	/*
   	 * returns true if the chain has no words
   	 */
   	public boolean isEmpty() {  	
         	return first == null;
   	}
   	
   	/*
   	 * getter for the first word
   	 */
   	public Word getFirstElement() {
         	// return first word
         	return this.first;
   	}
   	
   	/*
   	 * adds a word to the chain at the first position
   	 */
   	public void add(String newWord, long normalizedValue) {
         	// add a new word to the chain        	
         	if (isEmpty()) {
                	this.first = new Word (newWord, normalizedValue, null);
         	} else {
                	this.first = new Word (newWord, normalizedValue, this.first);
         	}
   	}
   	
   	/*
   	 * removes the first word of the chain
   	 */
   	public Word remove() {
         	// remove the first word from the chain
         	this.first = (Word) this.first.getNext();
         	
         	return this.first;
   	}
   	
   	/*
   	 * Returns a set of all words in the chain
   	 */
   	public HashSet<Word> getWords(){
   		HashSet<Word> words = new HashSet<Word>();
   		
   		if(first == null){
   			return words;
   		}   		
   		Word currentWord = first;
   		while (currentWord!=null){
   			words.add(currentWord);
   			currentWord=currentWord.getNext();
   		}
   		return words;
   	}
   	
   	/*
   	 * method for generating a nice string
   	 * @see java.lang.Object#toString()
   	 */
   	public String toString(){
   		if(first == null){
   			return "empty";
   		}
   		String toReturn="";
   		Word current = first;
   		while (current!=null){
   			toReturn+=current.toString()+", ";
   			current=current.getNext();
   		}
   		return toReturn;
   	}
   	
   	/*
   	 * returns the size of the Chain (number of words)
   	 */
   	public int size(){
   		int size = 0;
   		if(first == null){
   			return 0;
   		}   		
   		Word current = first;
   		while (current!=null){
   			size++;
   			current=current.getNext();
   		}
   		return size;
   	}
}